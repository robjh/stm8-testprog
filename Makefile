
all: main.ihx

main.ihx: main.c
	sdcc -mstm8 --std-c99 main.c
flash: main.ihx
#	stm8flash -c stlink -p stm8s003k3 -w main.ihx
	stm8flash -c stlinkv2 -p stm8s003k3 -w main.ihx
clean:
	find . -not -name *.c -not -name *.h -not -name Makefile -not -path "./.git*" -delete
