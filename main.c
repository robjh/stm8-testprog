// Source code under CC0 1.0
// attribution: http://www.colecovision.eu/stm8/STM8S-DISCOVERY%20Serial.shtml
#include <stdint.h>
#include <stdio.h>

// https://www.st.com/resource/en/datasheet/stm8s003k3.pdf
#define CLK_DIVR	(*(volatile uint8_t *)0x50c6)
#define CLK_PCKENR1	(*(volatile uint8_t *)0x50c7)

#define UART1_SR	(*(volatile uint8_t *)0x5230)
#define UART1_DR	(*(volatile uint8_t *)0x5231)
#define UART1_BRR1	(*(volatile uint8_t *)0x5232)
#define UART1_BRR2	(*(volatile uint8_t *)0x5233)
#define UART1_CR2	(*(volatile uint8_t *)0x5235)
#define UART1_CR3	(*(volatile uint8_t *)0x5236)

#define UART_CR2_TEN (1 << 3)
#define UART_CR3_STOP2 (1 << 5)
#define UART_CR3_STOP1 (1 << 4)
#define UART_SR_TXE (1 << 7)

enum PortIndex {
	PA,
	PB,
	PC,
	PD,
	PE,
	PF,
	PG,
	PH,
	PI,
};

typedef struct Port {
	uint8_t odr;
	uint8_t idr;
	uint8_t ddr;
	uint8_t cr1;
	uint8_t cr2;
} Port;

volatile Port * g_port = 0x5000;

typedef struct Pin {
	enum PortIndex port;
	uint8_t bit;
} Pin;
#define PIN(PORT, BIT) { PORT, (1 << BIT) }

inline void enable_output(const Pin* pinv, size_t pinc) {
	do {
		--pinc;
		g_port[pinv[pinc].port].ddr = g_port[pinv[pinc].port].ddr | pinv[pinc].bit;
		g_port[pinv[pinc].port].cr1 = g_port[pinv[pinc].port].cr1 | pinv[pinc].bit;
		g_port[pinv[pinc].port].cr2 = g_port[pinv[pinc].port].cr2 & ~(pinv[pinc].bit);
	} while(pinc);
}

enum ColourCombinations {
	COLOUR_BLACK,
	COLOUR_RED,
	COLOUR_GREEN,
	COLOUR_YELLOW,
	COLOUR_BLUE,
	COLOUR_MAGENTA,
	COLOUR_CYAN,
	COLOUR_WHITE,
	COLOUR_COUNT
};

const char * g_colour_combinations_s[] = {
	"black",
	"red",
	"green",
	"yellow",
	"blue",
	"magenta",
	"cyan",
	"white",
};

void set_pattern(const Pin* pinv, size_t pinc, enum ColourCombinations pattern) {
	// where each bit in pattern corresponds to a given output in pinv
	do {
		--pinc;
		if (pattern & (1 << pinc)) {
			g_port[pinv[pinc].port].odr &= ~(pinv[pinc].bit);
		} else {
			g_port[pinv[pinc].port].odr |= pinv[pinc].bit;
		}
	} while(pinc);
}

int putchar(int c) {
	while(!(UART1_SR & UART_SR_TXE));

	UART1_DR = c;
	return(c);
}


void main(void) {
	// The breakout board has a common-anode RGB LED on pins B6, B7 and F7 respectively.
	// This pins are physically next to each other, and don't offer anything in the way
	//   of interesting secondary/high speed features.
	const size_t pinc = 3;
	const Pin rgb[] = {
		PIN(PB, 6),
		PIN(PB, 7),
		PIN(PF, 4)
	};

	enable_output(rgb, pinc);

	CLK_DIVR = 0x00; // Set the frequency to 16 MHz
	CLK_PCKENR1 = 0xFF; // Enable peripherals

	UART1_CR2 = UART_CR2_TEN; // Allow TX and RX
	UART1_CR3 &= ~(UART_CR3_STOP1 | UART_CR3_STOP2); // 1 stop bit
	UART1_BRR2 = 0x03; UART1_BRR1 = 0x68; // 9600 baud

	for (;;) {
		for(enum ColourCombinations colour = COLOUR_BLACK; colour < COLOUR_COUNT; ++colour) {
			set_pattern(rgb, pinc, colour);
			printf("%s\r\n", g_colour_combinations_s[colour]);
			for(long int i = 0; i < 0x70000; i++); // Sleep
		}
	}
}

